<!--
*** This was build starting with the following README template:
*** https://github.com/othneildrew/Best-README-Template
*** This project uses a .md rather than .rst file as PyPi supports markdown
*** and is rendered better at the local GitLab/GitHub repository of the
*** source code.
-->

<!--
*** Using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->

<div align="center">
<p align="center">
    <a href="https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/-/blob/master/LICENSE.md">
        <img src="https://img.shields.io/badge/license-MIT%2FApache--2.0-green"
            alt="OSS License Info"></a>
    <a href="https://www.sphinx-doc.org/en/master/">
        <img src="https://img.shields.io/badge/made%20with-sphinx-blue"
            alt="Made with Sphinx"></a>
    <a href="https://pypi.org/project/sphinx-material-saltstack/">
        <img src="https://img.shields.io/pypi/dm/sphinx-material-saltstack?label=pypi%20downloads" 
            alt="PyPi download count"></a>
    <a href="https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/-/commits/master">
        <img src="https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/badges/master/pipeline.svg" 
            alt="GitLab CI Pipeline Status"></a>
    <br />
    <a href="https://saltstackcommunity.herokuapp.com/">
        <img src="https://img.shields.io/badge/slack-@saltstackcommunity-blue.svg?logo=slack"
            alt="chat on Slack"></a>
    <a href="https://www.twitch.tv/saltstackinc">
        <img src="https://img.shields.io/twitch/status/saltstackinc"
            alt="Watch on Twitch"></a>
    <a href="https://saltstackcommunity.herokuapp.com/">
        <img src="https://img.shields.io/reddit/subreddit-subscribers/saltstack?style=social"
            alt="chat on Slack"></a>
    <a href="https://twitter.com/intent/follow?screen_name=saltstack">
        <img src="https://img.shields.io/twitter/follow/saltstack?style=social&logo=twitter"
            alt="follow on Twitter"></a>
</p>
</div>

<!-- PROJECT LOGO -->
<div align="center">
<br />
<p align="center">
  <a href="https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack">
    <img src="/saltstack-customizations/sphinx_material_saltstack/static/images/salt-pdf-logo.png" alt="Logo" width="500">
  </a>

  <h3 align="center"><b>Sphinx Material Theme for SaltStack</b></h3>

  <a href="https://pypi.org/project/sphinx-material-saltstack/">
    <img src="https://img.shields.io/pypi/v/sphinx-material-saltstack"
        alt="Latest release version"></a>
  <br />
  A SaltStack-flavored spin of the <b><a href="https://bashtage.github.io/sphinx-material">Material for Sphinx</a></b> theme, making
  <br />
  <b><a href="https://www.sphinx-doc.org/en/master/">Sphinx</a></b> generated documentation look Salted!
  <br />
  <a href="https://saltstack.gitlab.io/open/docs/sphinx-material-saltstack"><strong>Explore the docs »</strong></a>
  <br />
  <a href="https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/issues">Report Bug</a>
  ·
  <a href="https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/issues">Request Feature</a>
  <br />
  <br />
</p>
</div>

>_**NOTE:** Any contributions to the theme, that are non-SaltStack specific,_
_should be done to the upstream Material for Sphinx (`sphinx-material`)_
_project, too. If accepted upstream, the feature can be reverted here_
_and inherited from upstream._



For examples of deployed documentation using this theme:
- [Demo Site](https://saltstack.gitlab.io/open/docs/sphinx-material-saltstack): Made from the `docs/*` directory of this project.
- [SaltStack Enterprise Installation Guide](https://enterprise.saltstack.com)

**Table of Contents**

<!-- TOC -->

- [Theme Differences from Sphinx Material](#theme-differences-from-sphinx-material)
    - [New Files](#new-files)
    - [Modified Files](#modified-files)
        - [License Differences](#license-differences)
- [Contributing](#contributing)
- [Installation](#installation)
    - [Interactive with pip](#interactive-with-pip)
    - [requirements.txt with pip](#requirementstxt-with-pip)
- [Configuration](#configuration)
    - [Optional: External Links Open New Tabs](#optional-external-links-open-new-tabs)
    - [Optional: Track Substitution Variables to Standalone File](#optional-track-substitution-variables-to-standalone-file)
    - [Optional: Have GitLab CI Generate a PDF of The Docs](#optional-have-gitlab-ci-generate-a-pdf-of-the-docs)
    - [Optional: Customizing or Overriding the layout.html](#optional-customizing-or-overriding-the-layouthtml)
- [Acknowledgments](#acknowledgments)

<!-- /TOC -->

## Theme Differences from Sphinx Material

This repository uses a git submodule of the upstream repo, and then both adds new content
and modifies existing content. This prevents this repository from hosting duplicate files
from the theme that this is a fork of, making for a cleaner repository that hosts only
the new and modified files of the original theme.

### New Files

This project is a downstream/derivative of the Material for Sphinx project. Completely new
additions to this project are the following files:

- `saltstack-customizations/sphinx_material_saltstack/static/images/favicon.png`
- `saltstack-customizations/sphinx_material_saltstack/static/images/saltstack-logo.png`
- `saltstack-customizations/sphinx_material_saltstack/static/images/salt-pdf-logo.png`
- `saltstack-customizations/sphinx_material_saltstack/static/stylesheets/saltstack.css`

### Modified Files

Otherwise, the following files are modified in this repository:

- `LICENSE.md`
  - See [License Differences](#license-differences)
- `docs/*` files
  - Since this is the example demo site, some modifications are made to the example docs.
- `setup.py`
  - Modifications referring to new package/plugin naming, and other small misc. changes.
- `saltstack-customizations/sphinx_material_saltstack/layout.html`
  - Made to also include `saltstack-customizations/sphinx_material_saltstack/static/stylesheets/saltstack.css`
- `saltstack-customizations/sphinx_material_saltstack/theme.conf`
  - Modified defaults that are specific to SaltStack preferences.

#### License Differences

This project is released under the **Apache 2.0** license, due to the
inclusion of trademark-related images. Specifically:

- `saltstack-customizations/sphinx_material_saltstack/static/images/favicon-salt.png`
- `saltstack-customizations/sphinx_material_saltstack/static/images/saltstack-logo.png`
- `saltstack-customizations/sphinx_material_saltstack/static/images/salt-pdf-logo.png`

The rest of the project is otherwise the same as the upstream project,
which is released under the **MIT** license. If wanting to make a derivative
of this theme, or include the source code elsewhere, it would be a better
idea to make use of the upstream theme source:

- [sphinx-material (on GitHub)](https://github.com/bashtage/sphinx-material/)

The `LICENSE` file of this repository includes both the content of the Apache 2.0
license, and the content of the original MIT license.

## Contributing

For contributing, take a look at the
[Contributing Guide](https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/-/blob/master/CONTRIBUTING.md).
Otherwise, the information below is more so for users wanting to
implement the theme in Sphinx-based documentation projects.

## Installation

Setup a `venv`:

```bash
python3 -m venv .venv
source .venv/bin/activate
pip install -U pip setuptools wheel
```

### Interactive with pip

Install via `pip` in a documentation project:

```bash
pip install sphinx-material-saltstack
```

### requirements.txt with pip

Add the following to a `requirements.txt` file:

```
sphinx-material-saltstack
```

Install from `pip`:

```
pip install -r requirements.txt
```

## Configuration

> ***NOTE:*** _Refer to the example docs configuration in `docs/conf.py` for_
_further information, and a more in-depth example to compare against._

Add the following to your imports in `conf.py`:

```python
import sphinx_material_saltstack
```

To get the minimum defaults, including SaltStack logos, ensure the following
is within the `conf.py`:

```python
# Base Material Theme requirements
html_show_sourcelink = True  # False is wanting to hide "Show Source" button for rst sourcecode
html_theme = "sphinx_material_saltstack"
html_theme_path = sphinx_material_saltstack.html_theme_path()
html_context = sphinx_material_saltstack.get_html_context()
html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "localtoc.html", "searchbox.html"]
}
html_theme_options = {
    # Set the name of the project to appear in the navigation.
    "nav_title": project,
    # Set you GA account ID to enable tracking
    # "google_analytics_account": "",
    # Set the repo location to get a badge with stats (only if public repo)
    "repo_url": "https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/",
    "repo_name": "SaltStack Material on GitLab",
    # Repo type supports gitlab or github
    "repo_type": "gitlab",
    # Visible levels of the global TOC; -1 means unlimited
    "globaltoc_depth": 1,
    # If False, expand all TOC entries
    "globaltoc_collapse": False,
    # If True, show hidden TOC entries
    "globaltoc_includehidden": True,
    # hide tabs?
    "master_doc": False,
    # Minify for smaller HTML/CSS assets
    "html_minify": False, # Change to True if very large doc set
    "html_prettify": True, # Change to False if very large doc set
    "css_minify": False, # Change to True if very large doc set
}

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
# This is how to properly include the built-in SaltStack logo in any documentation site.
html_logo = os.path.join(
    html_theme_path[0],
    "sphinx_material_saltstack",
    "static",
    "images",
    "saltstack-logo.png",
)

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large. Favicons can be up to at least 228x228. PNG
# format is supported as well, not just .ico'
# This is how to properly include the built-in SaltStack logo in any documentation site.
html_favicon = os.path.join(
    html_theme_path[0], "sphinx_material_saltstack", "static", "images", "favicon.png",
)
```

### Optional: External Links Open New Tabs

For external links to open a new tab by default, add the following below your
imports in the `conf.py` file.

Add to the imports section:

```python
from docutils import nodes
from docutils.nodes import Element
from sphinx.writers.html import HTMLTranslator
```

Add below the imports section:

```python
# Force all external links to open as new tabs,
# without breaking internal links. This causes
# external links to work by default on HTML
# generated sites, while natively working in PDF
# output, also.
#
# Overwrites visit_reference() Sphinx method found
# in HTMLTranslator class of sphinx/sphinx/writers/html.py
# Solution Source: https://stackoverflow.com/a/61669375/5340149
class PatchedHTMLTranslator(HTMLTranslator):
    def visit_reference(self, node: Element) -> None:
        atts = {"class": "reference"}
        if node.get("internal") or "refuri" not in node:
            atts["class"] += " internal"
        else:
            atts["class"] += " external"
            # Customize behavior (open in new tab, secure linking site)
            atts["target"] = "_blank"
            atts["rel"] = "noopener noreferrer"
        if "refuri" in node:
            atts["href"] = node["refuri"] or "#"
            if self.settings.cloak_email_addresses and atts["href"].startswith(
                "mailto:"
            ):
                atts["href"] = self.cloak_mailto(atts["href"])
                self.in_mailto = True
        else:
            assert (
                "refid" in node
            ), 'References must have "refuri" or "refid" attribute.'
            atts["href"] = "#" + node["refid"]
        if not isinstance(node.parent, nodes.TextElement):
            assert len(node) == 1 and isinstance(node[0], nodes.image)
            atts["class"] += " image-reference"
        if "reftitle" in node:
            atts["title"] = node["reftitle"]
        if "target" in node:
            atts["target"] = node["target"]
        self.body.append(self.starttag(node, "a", "", **atts))

        if node.get("secnumber"):
            self.body.append(
                ("%s" + self.secnumber_suffix) % ".".join(map(str, node["secnumber"]))
            )


# Run above custom function against links
def setup(app):
    app.set_translator("html", PatchedHTMLTranslator)
```

### Optional: Track Substitution Variables to Standalone File

> ***NOTE:*** _This feature assumes that the Sphinx project is using `.rst`_
_formatted files for documentation instead of `.md` or Markdown. This has not_
_been tested with `.md`, which has less functionality and support in Sphinx_
_documentation projects. If still wanting to use Markdown instead, there is a_
_promising project trying to bring feature-parity to Markdown, seen here:_
_[MyST-Parser](https://github.com/executablebooks/MyST-Parser)._

By default, many projects throw all Sphinx project configurations into the
`conf.py` file, making it rather large. Some projects can benefit from
separating out some of the content into dedicated files.

If you'd like to store substitution variables, that can replace placeholders
when HTML and/or PDFs are built, follow these steps.

- Create a new file at `docs/sitevars.rst`
- Using this example format, `|previous_release_version|` will be replaced in
  `.rst` files with `v1.1.0` if this is saved in `docs/sitevars.rst`.

  ```rst
  .. |previous_release_version| replace:: 1.1.0
  ```

  It can also be used to insert an image that is used more than once!

  ```rst
  .. |speical_icon| image:: /_static/images/special-icon.png
  ```

- Make sure to add the file to the `exclude_patterns` list.

  ```python
  exclude_patterns = [
    ...
    "sitevars.rst",
    ...
  ]
  ```

- Define `rst_prolog` in `docs/conf.py` to import the values.

  ```python
  # Variables to pass into the docs from sitevars.txt for rst substitution
  with open("sitevars.rst") as site_vars_file:
      site_vars = site_vars_file.read().splitlines()

  rst_prolog = """
  {}
  """.format(
      "\n".join(site_vars[:])
  )
  ```

### Optional: Have GitLab CI Generate a PDF of The Docs

If you find yourself needing to create a PDF of your documentation, Spinx uses
LaTeX to convert documentation into PDFs. Though, it requires several
dependencies and can be painful to get different systems to properly work.

To make life easy, a docker container can be built as part of the GitLab CI
pipeline (or CI pipeline of choice) that contains all the dependencies. It can
then be used later in the pipeline to generate a PDF of your project.

- Copy `docs/pdfcontainer/Dockerfile` into your project.
- Add the following to your `Makefile`

  ```makefile
  ```

- Add the following to your `.gitlab-ci.yml` file.

  ```yml
  ```

> ***NOTE:*** _If your project doesn't have a `.gitlab-ci.yml` file, use the_
_`.gitlab-ci.yml` file within the source code repository of this project as a_
_reference. It can be likely be copied over with only minimal modifications in_
_order to achieve what you need._

### Optional: Customizing or Overriding the `layout.html`

> ***NOTE:*** _This is a more involved/advanced feature of Sphinx. Use only if_
_needed, and only if a project requires additional functionality that this_
_theme can't provide already. If this theme should have an additional feature_
_that other projects could benefit from, open an issue about it or submit an MR_
_to this repository after reading the_
_[Contributing Guide](https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/-/blob/master/CONTRIBUTING.md)._

You can customize the theme by overriding Jinja template blocks. For
example, `layout.html` contains several blocks that can be overridden
or extended.

Create a new `layout.html` file in your own project's `docs/_templates`
directory.

```bash
mkdir docs/_templates
touch docs/_templates/layout.html
```

Then, configure your `conf.py`:

```python
templates_path = ['_templates']
```

Finally, edit your override file `docs/_templates/layout.html`:

```jinja2
    {# Import the theme's layout. #}
    {% extends '!layout.html' %}

    {%- block extrahead %}
    {# Add custom things to the head HTML tag #}
    {# Call the parent block #}
    {{ super() }}
    {%- endblock %}
```

If needed, you can create a local duplicate of the `layout.html` file found inside
this repository, and make changes directly to it in order to override/replace
the built-in `layout.html` when the theme is imported.

## Acknowledgments

* [Img Shields](https://shields.io) for making repository badges easy
