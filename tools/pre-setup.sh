# Build base from sphinx-material submodule
cp -R lib/sphinx-material/sphinx_material sphinx_material_saltstack
mv sphinx_material_saltstack/sphinx_material sphinx_material_saltstack/sphinx_material_saltstack

# Overwrite with SaltStack specific customizations
cp saltstack-customizations/*.py sphinx_material_saltstack
cp -R saltstack-customizations/sphinx_material_saltstack sphinx_material_saltstack
rm sphinx_material_saltstack/_version.py