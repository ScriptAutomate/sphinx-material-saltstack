# Contributing

Welcome! If you're interested in contributing to this project, you're in the
right place.

When contributing to this repository, best practice is to first discuss the
change you wish to make via an issue (preferred!) and/or a message in the
`#documentation` channel of the
[SaltStack Community Slack](https://saltstackcommunity.herokuapp.com/).

> Please note we have a
[Code of Conduct](https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/-/blob/master/CODE_OF_CONDUCT.md).
Please follow it in all your interactions with the project.

<!-- TOC -->

- [Contributing](#contributing)
    - [Different Paths to Contribute](#different-paths-to-contribute)
    - [Prerequisites](#prerequisites)
    - [Contributing Workflow](#contributing-workflow)
    - [Local Development](#local-development)
        - [Python venv](#python-venv)
            - [What is pre-commit?](#what-is-pre-commit)
        - [Test out Local Changes](#test-out-local-changes)
    - [Preparing for Submitting an MR](#preparing-for-submitting-an-mr)
            - [Additional Notes on MRs](#additional-notes-on-mrs)
    - [Get To It!](#get-to-it)
        - [SaltStack Community Links](#saltstack-community-links)

<!-- /TOC -->

## Different Paths to Contribute

There are multiple ways to contribute an update to this project. Choose the best
path for your needs:

- Open a new issue, or contribute to the discussions of existing issues.
- Use the [GitLab Web Editor](https://docs.gitlab.com/ee/user/project/repository/web_editor.html) 
  to make simple changes to a **Fork**, and submit an MR.
  - You can optionally use the [GitLab Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/).
- Clone the repository locally to a developer workstation, and contribute with
  your favorite IDE, while visually verifying the results of your changes.

If cloning the repository locally, for more complex changes, then follow the
rest of the guide.

## Prerequisites

For local development, the following prerequisites are needed:

- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
- [Python 3.6+](https://realpython.com/installing-python/)
- [Ability to create python venv](https://realpython.com/python-virtual-environments-a-primer/)

## Contributing Workflow

A Pull Request (PR) is known as a Merge Request (MR) on GitLab. They will be
referred to as MRs throughout this guide.

This project uses a _**Fork** -> **Branch** -> **MR**_ to `master` branch
approach to development.

- [Fork this repository](https://gitlab.com/saltstack/open/docs/sphinx-material-saltstack/-/forks/new).
- Clone your fork to your workstation.

  ```bash
  git clone <your-gitlab-fork>
  ```

## Local Development

This project is a Sphinx theme, and Sphinx has documentation specific to
developing these kinds of projects:
- [Sphinx HTML theme development](https://www.sphinx-doc.org/en/3.x/development/theming.html)

Since this is a spin on the upstream
[sphinx-material](https://www.sphinx-doc.org/en/3.x/development/theming.html)
theme, it uses a **git submodule** to provide the upstream base files during
development and build/publishing of the packaged theme.

- Pull in submodule files for `sphinx-material`.

  ```bash
  git submodule update --init --recursive
  ```

- Create a new branch for your contribution before making your changes.

  ```bash
  git checkout -b my-awesome-feature
  ```

### Python `venv`

Setup a local Python `venv`:

```bash
# Setup venv
python3 -m venv .venv
# If Python 3.6+ is in path as 'python', use the following instead:
# python -m venv .venv

# Activate venv
source .venv/bin/activate

# Install required python packages to venv
pip install -U pip setuptools wheel
pip install -r requirements-dev.txt
pip install -r requirements.txt

# Setup pre-commit
pre-commit install
```

#### What is pre-commit?

[**pre-commit**](https://pre-commit.com/) is a tool that will automatically run local tests when you attempt
to make a git commit. To view what tests are run, you can view the
`.pre-commit-config.yaml` file at the root of the repository.

One big benefit of pre-commit is that _auto-corrective measures_ can be done
to files that have been updated. This includes Python formatting best practices,
proper file line-endings (which can be a problem with repository contributors
using differing operating systems), and more.

If an error is found that cannot be automatically fixed, error output will help
point you to where an issue may exist. For example, `codespell` may find words
it thinks have been spelled incorrectly.

```
# Example output
Check for merge conflicts................................................Passed
Mixed line ending........................................................Passed
Fix End of Files.........................................................Passed
Check python ast.........................................................Passed
Check Yaml...............................................................Passed
Detect Private Key.......................................................Passed
black....................................................................Passed
codespell................................................................Passed
- hook id: codespell
- exit code: 1

README.md:232: wrong-word ==> suggested-word
```

In this example above, `codespell` flagged a word that it believes has been
spelled incorrectly:

```
README.md:232: wrong-word ==> suggested-word
```

Going back to the source and fixing the word will fix the problem, and the next
run will pass:

```
# Example output
Check for merge conflicts................................................Passed
Mixed line ending........................................................Passed
Fix End of Files.........................................................Passed
Check python ast.........................................................Passed
Check Yaml...............................................................Passed
Detect Private Key.......................................................Passed
black....................................................................Passed
codespell................................................................Passed
```

A GitLab CI job also runs pre-commit tests in the repository in case the
contributor has not configured it locally.

### Test out Local Changes

Run the following to prepare a local version of the theme package:

```bash
# This takes the submodule, creates a folder called sphinx_material_saltstack,
## and overwrites files with what is found in saltstack-customizations
./tools/pre-setup.sh
```

To actively test changes from the theme in development:

```bash
pip install -e .
cd docs
make html
```

The example site should now be located in `docs/_build/html`. For easy viewing,
run:

```bash
google-chrome _build/html/index.html
```

## Preparing for Submitting an MR

Setup the `upstream` **remote** for easy future syncing.

```bash
# Using ssh for remote
git remote add upstream git@gitlab.com:saltstack/open/docs/sphinx-material-saltstack.git
```

If wanting to sync updates from upstream to your fork, do the following:

```bash
git checkout master
git fetch upstream
git pull upstream master
git push origin master
```

To merge updates from `master` into an active branch, which may be needed when
there are potential merge conflicts, do the following:

```bash
git checkout my-awesome-feature
git merge master
```

When changes are ready, create a new Merge Request!

#### Additional Notes on MRs

- Ensure any install or build dependencies are ignored and not included in an
  MR/PR.
- Update the README.md with details of changes to the interface, this includes
  new environment variables, exposed ports, useful file locations and container
  parameters.

## Get To It!

The Salt development team is welcoming, positive, and dedicated to helping
people get new code and fixes into SaltStack projects. Log into GitHub or GitLab
and get started with one of the largest developer communities in the world. The
following links should get you started:

- [SaltStack Projects: GitLab-hosted](https://gitlab.com/saltstack)
- [SaltStack Projects: GitHub-hosted](https://github.com/saltstack)

### SaltStack Community Links

- [SaltStack Community Slack](https://saltstackcommunity.herokuapp.com/)
- [SaltStack StackOverflow](https://stackoverflow.com/questions/tagged/salt-stack%20or%20salt-cloud%20or%20salt-creation%20or%20salt-contrib?sort=Newest&edited=true)
- [SaltStack Subreddit](https://www.reddit.com/r/saltstack/)
- [SaltStack on LinkedIn](https://www.linkedin.com/company/salt-stack-inc/)
- [SaltStack on Twitter](https://twitter.com/SaltStack)
- [SaltStack on Facebook](https://www.facebook.com/SaltStack/)